module.exports = {
  development: {
    dialect: 'sqlite',
    storage: './db.development.sqlite'
  },
  test: {
    dialect: 'sqlite',
    storage: ':memory:'
  },
  production: {
    username: 'postgres',
    password: 'mysecretpassword',
    database: 'example',
    host: 'localhost',
    port: 25060,
    dialect: 'postgres',
    ssl: true,
      "dialectOptions": {
      "ssl": true
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },

    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    operatorsAliases: false
  }
};
