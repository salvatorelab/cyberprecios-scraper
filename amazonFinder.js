var config = require('./config.json');
var parseString = require('xml2js').parseString;
var OperationHelper = require('apac').OperationHelper;
var opHelper = new OperationHelper(config.amazon);

opHelper.execute('ItemLookup', {
  //search by EAN
//  'SearchIndex': 'All',
//  'IdType': 'EAN',
//  'ItemId': '8713439210811',

  //search by ASIN
  'IdType': 'ASIN',
  'ItemId': 'B07H1GZ9YH',


  'ResponseGroup': 'ItemAttributes,Images,VariationSummary,Offers'
}).then((response) => {
    //console.log("Results object: ", response.result);
    //console.log("Raw response body: ", response.responseBody);
    parseString(response.responseBody, function (err, result) {
      if (err) reject(err);
      else {
        var amazonProducts = result.ItemLookupResponse.Items[0].Item;
        var amazonProduct = amazonProducts[0];
        console.log(JSON.stringify(amazonProduct));

        //console.dir(amazonProduct.ItemAttributes[0].ListPrice);
        console.log(bestPrice(amazonProduct));

      }

    });
});

function bestPrice(amazonProduct) {
  var price = null;
  var offer = null;

  if (amazonProduct.ItemAttributes[0].ListPrice) {
    price = amazonProduct.ItemAttributes[0].ListPrice[0].Amount[0]/100;
  }

  if (amazonProduct.Offers && amazonProduct.Offers[0].Offer && amazonProduct.Offers[0].Offer[0].OfferListing) {
    if (amazonProduct.Offers[0].Offer[0].OfferAttributes[0].Condition == 'New') {
      offer = amazonProduct.Offers[0].Offer[0].OfferListing[0].Price[0].Amount[0]/100;
    }
  }

  if (offer) return offer;
  return price;
}
