'use strict';

var models = require('./models');

function connect() {
  return models.sequelize.sync();
}

function close() {
  models.sequelize.close();
}

module.exports = {
  'connect': connect,
  'close': close
};
